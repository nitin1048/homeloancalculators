Selenium-cucumber : Automation Testing Using Java

Selenium-cucumber is a behavior driven development (BDD) approach to write automation test script to test Web. It enables you to write and execute automated acceptance/unit tests. It is cross-platform, open source and free. Automate your test cases with minimal coding. More Details


#Requirements

1. Set up JDK on system (JDK 1.8 or the latest version)
2. Install Eclipse (Eclipse OXYGEN or the latest version)
3. Selenium-server-standalone(Can be downloaded at http://www.seleniumhq.org/download/)
4. Jar files For Cucumber  (Can be downloaded at http://mvnrepository.com/search?q=Cucumber)
	a.Cucumber-core
	b.Cucumber-html
	c.cobertura code coverage
	d.Cucumber-java
	e.Cucumber-junit
	f.Cucumber-jvm-deps
	g.Cucumber-reporting
	h.Hemcrest-core
	i.Gherkin
	j.Junit
5. Install Maven



#Maven Setup:
Step 1: To setup Maven, download the maven�s latest version form Apache depending upon different OS.
Step 2: Unzip the folder and save it on the local disk.
Step 3: Create environment variable for MAVEN_HOME. Follow the below step:
        Navigate to System Properties ->Advanced System Settings>Environment Variable ->
        System Variable ->New ->Add path of Maven folder
Step 4: Edit path variable and provide the bin folder path.
Step 5: Now verify the maven installation using command prompt (Use mvn �version) 



Download a Framework
Maven - https://github.com/selenium-cucumber/selenium-cucumber-java-maven-example

Framework Architecture :

HomeLoanCalculator
	|
	|_src/main/java
	|	|_core
	|	|	|_Action.java
	|	|	|_Base.java
	|	|_homeLoanCalculator
	|	|	|_HomeLoan.java
	|	|	
	|	|_objects
	|		|_HomeLoanCalculatorObjects.java
	|_src/test/java
	|	|_features
	|	|	|_homeLoanCalculator.feature
	|	|_runners
	|	|	|_TestRunner.java
	|	|_stepDefinitions
	|	|	|_TestHomeLoanCalculator.java
	
	
src/test/java/features - All the cucumber features files (files .feature ext) goes here.
src/test/java/stepDefinitions - We can define step defintion under this package for your feature steps.


#Running test
Go to your project directory from terminal and hit following commands

mvn test (defualt will run on local browser)
mvn test "-Dbrowser=chrome" (to use any other browser)
mvn test -Dcucumber.options="classpath:features/homeLoanCalculator.feature" to run specific feature.
mvn test -Dcucumber.options="�-plugin html:target/cucumber-reports/report.html" to generate a HTML report.
mvn test -Dcucumber.options="�-plugin json:target/cucumber-reports/report.json" to generate a JSON report.



