package homeLoanCalculator;

import static core.Action.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertNotEquals;
import static objects.HomeLoanCalculatorObjects.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import core.Base;

public class HomeLoan extends Base {

	public static void loginPage(WebDriver driver, String wert) throws Exception {
		driver.get(wert);
	}

	public static void selectSingleTabInHomeLoanCalculatorPage(WebDriver driver) throws Exception {
		assertTrue(" Single button text is not displaying",verifyElementPresent(driver, By.xpath(singleButton_Xpath)));
		assertTrue(" Join button text is not displaying",verifyElementPresent(driver, By.xpath(jointButton_Xpath)));
		click(driver, By.xpath(singleButton_Xpath));
	}

	public static void selectDependentInHomeLoanCalculatorPage(WebDriver driver) throws Exception {
		assertTrue(" Numbers of dependants dropdown is not displaying",verifyElementPresent(driver, By.xpath(dependants_Xpath)));
		click(driver, By.xpath(dependants_Xpath));
		assertTrue(" 0 dependants  is not displaying",verifyElementPresent(driver, By.xpath(dependants_0_Xpath)));
		click(driver, By.xpath(dependants_0_Xpath));
	}

	public static void selectHomeToLiveInTab(WebDriver driver) throws Exception {
		assertTrue("  Home to live in button is not displaying",
		verifyElementPresent(driver, By.xpath(homeToLiveButton_Xpath)));
		assertTrue("  Residential Investment button is not displaying",verifyElementPresent(driver, By.xpath(resdentialInvestmentButton_Xpath)));
		click(driver, By.xpath(homeToLiveButton_Xpath));
	}

	public static void enterAmountInYourIncomeField(WebDriver driver, String myincome) throws Exception {
		assertTrue("  Currency icon ($) is not displaying",verifyElementPresent(driver, By.xpath(yourIncomeCurrency_Xpath)));
		assertTrue("  Year text is not displaying",verifyElementPresent(driver, By.xpath(yourIncomeYear_Xpath)));
		driver.findElement(By.xpath(yourIncomeField_Xpath)).sendKeys(myincome);
	}

	public static void enterAmountInOtherIncomeField(WebDriver driver, String otherIncome) throws Exception {
		assertTrue("  Currency icon ($) is not displaying",verifyElementPresent(driver, By.xpath(otherIncomeCurrency_Xpath)));
		assertTrue("  Year text is not displaying",verifyElementPresent(driver, By.xpath(otherIncomeYear_Xpath)));
		driver.findElement(By.xpath(otherIncomeField_Xpath)).sendKeys(otherIncome);
	}

	public static void verifyHeaderYourExpenses(WebDriver driver) throws Exception {
		assertTrue("Your expenses header is not displaying",verifyElementPresent(driver, By.xpath(yourExpensesHeader_Xpath)));
	}

	public static void enterDataInLivingExpensesField(WebDriver driver, String livingExpenses) throws Exception {
		assertTrue("  Currency icon ($) is not displaying", verifyElementPresent(driver, By.xpath(livingExpensesCurrency_Xpath)));
		assertTrue("  Year text is not displaying",	verifyElementPresent(driver, By.xpath(livingExpensesMonth_Xpath)));
		driver.findElement(By.id(livingExpensesField_ID)).sendKeys(livingExpenses);
	}

	public static void enterAmountInCurrentHomeLoanRepaymentsField(WebDriver driver, String homeloans) throws Exception {
		assertTrue("  Currency icon ($) is not displaying",	verifyElementPresent(driver, By.xpath(currentHomeLoanRepaymentsCurrency_Xpath)));
		assertTrue("  Year text is not displaying",verifyElementPresent(driver, By.xpath(currentHomeLoanRepaymentsMonth_Xpath)));
		driver.findElement(By.id(currentHomeLoanRepaymentsField_ID)).sendKeys(homeloans);
//		sendKeys(driver, By.id(currentHomeLoanRepaymentsField_ID), homeloans);
	}

	public static void enterAmountInOthetrsRepaymentsField(WebDriver driver, String otherloans) throws Exception {
		assertTrue(" Currency icon ($) is not displaying", verifyElementPresent(driver, By.xpath(othersRepaymentsCurrency_Xpath)));
		assertTrue(" Year text is not displaying", verifyElementPresent(driver, By.xpath(othersRepaymentsMonth_Xpath)));
		driver.findElement(By.id(othersRepaymentsField_ID)).sendKeys(otherloans);
	}

	public static void enterAmountInOthetCommitmentField(WebDriver driver, String otherCommitments) throws Exception {
		assertTrue(" Currency icon ($) is not displaying", verifyElementPresent(driver, By.xpath(othersCommitmentsCurrency_Xpath)));
		assertTrue(" Year text is not displaying", verifyElementPresent(driver, By.xpath(othersCommitmentMonth_Xpath)));
		driver.findElement(By.xpath(othersCommitmentField_Xpath)).sendKeys(otherCommitments);
	}
	
	public static void enterAmountInTotalCreditCardLimitsField(WebDriver driver, String creditCardAmount) throws Exception {
		assertTrue(" Currency icon ($) is not displaying", verifyElementPresent(driver, By.xpath(totalCreditCardLimitsCurrency_Xpath)));
		driver.findElement(By.id(totalCreditCardLimitsField_ID)).sendKeys(creditCardAmount);
	}

	public static void clickOnWorkOutHowMuchICouldBorrowButton(WebDriver driver) throws Exception {
		assertTrue(" Work out how much I could borrow text is not displaying", verifyElementPresent(driver, By.xpath(workOutHowMuchICouldBorrowButton_Xpath)));
		click(driver, By.xpath(workOutHowMuchICouldBorrowButton_Xpath));
		waitTill();
	}

	public static void verifyTheBorrowEstimate(WebDriver driver) throws Exception {
		assertTrue(" Money Bag Icon is not displaying", verifyElementPresent(driver, By.xpath(moneyBagIcon_Xpath)));
		assertTrue(" We estimate you could borrow: text is not displaying",	verifyElementPresent(driver, By.xpath(estimateYocouldborrowtext_Xpath)));
		assertTrue(" Estimated Amount is not displaying", verifyElementPresent(driver, By.xpath(estimatedAmountText_Xpath)));
		waitTill();
		String money = getText(driver, By.xpath(estimatedAmountText_Xpath));
		assertEquals(" Not getting estimate value", money, estimatedAmount);
	}
	
	

	public static void verifyAllHeaderInHomePage(WebDriver driver, String elementValue) throws Exception {
		assertTrue(elementValue + " header is not displaying", verifyElementPresent(driver, By.xpath("//h3[text()='" + elementValue + "']")));
//		assertTrue(elementValue + " header is not displaying", verifyElementPresent(driver, By.xpath(header_Xpath)));
	}

	public static void verifyAllLabelInHomePage(WebDriver driver, String elementValue) throws Exception {
		assertTrue(elementValue + " label is not displaying", verifyElementPresent(driver, By.xpath("//label[text()='" + elementValue + "']")));
	}

	public static void clickOnStartOverIcon(WebDriver driver) throws Exception {
		assertTrue(" Start Over Icon is not displaying", verifyElementPresent(driver, By.xpath(startOverIcon_Xpath)));
		assertTrue(" Start Over text is not displaying", verifyElementPresent(driver, By.xpath(startOverButton_Xpath)));
		click(driver,By.xpath(startOverButton_Xpath));
	}
	
	public static void verifyAmountAfterClearingFields(WebDriver driver) throws Exception {
		String yourIncome = driver.findElement(By.xpath(yourIncomeField_Xpath)).getAttribute("value");
		String otherIncome = driver.findElement(By.xpath(otherIncomeField_Xpath)).getAttribute("value");
		String livingExpenses = driver.findElement(By.id(livingExpensesField_ID)).getAttribute("value");
		String otherloans = driver.findElement(By.id(othersRepaymentsField_ID)).getAttribute("value");
		String creditCardAmount = driver.findElement(By.id(totalCreditCardLimitsField_ID)).getAttribute("value");
		clickOnStartOverIcon(driver);
		waitTill();
		String yourIncome1 = driver.findElement(By.xpath(yourIncomeField_Xpath)).getAttribute("value");
		String otherIncome1 = driver.findElement(By.xpath(otherIncomeField_Xpath)).getAttribute("value");
		String livingExpenses1 = driver.findElement(By.id(livingExpensesField_ID)).getAttribute("value");
		String otherloans1 = driver.findElement(By.id(othersRepaymentsField_ID)).getAttribute("value");
		String creditCardAmount1 = driver.findElement(By.id(totalCreditCardLimitsField_ID)).getAttribute("value");
		assertNotEquals(" yourIncome field amount is not cleared after clicking on Start over button", yourIncome,yourIncome1);
		assertNotEquals(" otherIncome field amount is not cleared after clicking on Start over button", otherIncome,otherIncome1);
		assertNotEquals(" livingExpenses field amount is not cleared after clicking on Start over button",livingExpenses, livingExpenses1);
		assertNotEquals(" otherloans field amount is not cleared after clicking on Start over button", otherloans,otherloans1);
		assertNotEquals(" creditCardAmount field amount is not cleared after clicking on Start over button",creditCardAmount, creditCardAmount1);
	}
	

	public static void verifytheMessage(WebDriver driver) throws Exception {
		waitForElement(driver, By.xpath(message_Xpath));
		assertTrue(" Message is not displaying",verifyElementPresent(driver, By.xpath(message_Xpath)));
		assertTrue(" Contact number is not displaying",verifyElementPresent(driver, By.xpath(contactDeatils_Xpath)));
	}
}
