package core;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import com.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import  cucumber.api.java.After;
import cucumber.api.java.Before;

public class Base {
	  public static  WebDriver driver;
	  public static String default_browser = "chrome";
	  static String os = System.getProperty("os.name");

	    @Before
		public void setUp() throws Exception{
		  if(default_browser.equalsIgnoreCase("chrome")){ 
				if(os.equalsIgnoreCase("mac os x")){
					System.setProperty("webdriver.chrome.driver", "chromedriver");
		            ChromeOptions options = new ChromeOptions();
		            options.addArguments("--test-type");
		            options.addArguments("start-maximized");
		            driver = new ChromeDriver(options);
				}else{
					System.out.println("Launching browser");
					System.setProperty("webdriver.chrome.driver","chromedriver.exe");
				    ChromeOptions options = new ChromeOptions();
				    options.addArguments("chrome.switches","--disable-extensions");
				    options.addArguments("--test-type");        
				    options.addArguments("start-maximized");    
				    options.addArguments("disable-infobars");
					driver = new ChromeDriver(options);         
				}
			} 
	}
	    @Before(order = 1)
	    public void beforeScenario(Scenario scenario) {
			Reporter.assignAuthor("Nitin_Singh");
	    }

			public static String getCurrentTime(){
				    Calendar cal = Calendar.getInstance();
				    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				    String currentTime = date.format(cal.getTime());
					return currentTime;
			}
			
			@After
			public void failedScenarioScreenShots(Scenario scenario) {
				if (scenario.isFailed()) {
					String curr_time = getCurrentTime();
					curr_time = curr_time.replace(" " , "");
					curr_time  = curr_time.replace(":", "");
					String screenshotName = scenario.getName().replaceAll(" ", "_");
					screenshotName = screenshotName+curr_time;
					try {
						File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/" + screenshotName + ".png");
						FileUtils.copyFile(srcFile, destinationPath);  
						Reporter.addScreenCaptureFromPath(destinationPath.toString());
						System.out.println("---------------ENDED BROWSER----------------------"); 
						driver.quit();
					} catch (IOException e) {
					} 
				}
				else {
					System.out.println("---------------Sucess----------------------"); 
					System.out.println("---------------ENDED BROWSER----------------------"); 
					driver.quit();
	
				}
			
			}
}

