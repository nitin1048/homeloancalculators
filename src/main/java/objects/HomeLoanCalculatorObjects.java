package objects;

public class HomeLoanCalculatorObjects {
	
	public static String singleButton_Xpath = "//label[@for='application_type_single']";
	public static String jointButton_Xpath = "//label[@for='application_type_joint']";
	public static String dependants_Xpath = "//select[@title='Number of dependants']";
	public static String dependants_0_Xpath = "//select[@title='Number of dependants']/option[1]";
	public static String homeToLiveButton_Xpath = "//label[@for='borrow_type_home']";
	public static String resdentialInvestmentButton_Xpath = "//label[@for='borrow_type_investment']";
	public static String yourIncomeCurrency_Xpath = "//span[@class='currency' and @id='q2q1i1' and text()='$']";
	public static String yourIncomeYear_Xpath = "//span[@class='year' and @id='q2q1i2' and text()='/ year']";
	public static String yourIncomeField_Xpath = "//input[@aria-labelledby='q2q1']";
	public static String otherIncomeCurrency_Xpath = "//span[@class='currency' and @id='q2q2i1' and text()='$']";
	public static String otherIncomeYear_Xpath = "//span[@class='year' and @id='q2q2i2' and text()='/ year']";
	public static String otherIncomeField_Xpath = "//input[@aria-labelledby='q2q2']";
	public static String yourExpensesHeader_Xpath = "//h3[text()='Your expenses']";
	public static String livingExpensesCurrency_Xpath = "//span[@class='currency' and @id='q3q1i1' and text()='$']";
	public static String livingExpensesMonth_Xpath = "//span[@class='year' and @id='q3q1i2' and text()='/ month']";
	public static String livingExpensesField_ID = "expenses";
	public static String currentHomeLoanRepaymentsCurrency_Xpath = "//span[@class='currency' and @id='q3q2i1' and text()='$']";
	public static String currentHomeLoanRepaymentsMonth_Xpath = "//span[@class='year' and @id='q3q2i2' and text()='/ month']";
	public static String currentHomeLoanRepaymentsField_ID = "homeloans";
	public static String othersRepaymentsCurrency_Xpath = "//span[@class='currency' and @id='q3q3i1' and text()='$']";
	public static String othersRepaymentsMonth_Xpath = "//span[@class='year' and @id='q3q3i2' and text()='/ month']";
	public static String othersRepaymentsField_ID = "otherloans";
	public static String othersCommitmentsCurrency_Xpath = "//span[@class='currency' and @id='q3q4i1' and text()='$']";
	public static String othersCommitmentMonth_Xpath = "//span[@class='year' and @id='q3q4i2' and text()='/ month']";
	public static String othersCommitmentField_Xpath = "//input[@aria-labelledby='q3q4']";
	public static String totalCreditCardLimitsCurrency_Xpath = "//span[@class='currency' and @id='q3q5i1' and text()='$']";
	public static String totalCreditCardLimitsField_ID = "credit";
	public static String workOutHowMuchICouldBorrowButton_Xpath = "//button[text()='Work out how much I could borrow']";
	public static String moneyBagIcon_Xpath = "//span[@class='icon icon_moneybag_loan']";
	public static String estimateYocouldborrowtext_Xpath = "//span[contains(.,'We estimate you could borrow:')]";
	public static String estimatedAmountText_Xpath = "//span[@class='borrow__result__text__amount']";
	public static String estimatedAmount = "$479,000";
	public static String startOverIcon_Xpath = "//div[@class='borrow__result text--white clearfix']//span[@class='icon icon_restart']";
	public static String startOverButton_Xpath = "//div[@class='borrow__result text--white clearfix']//button[@ aria-label='Start over']";
	public static String message_Xpath = "//span[contains(.,'Based on the details')]";
	public static String contactDeatils_Xpath = "//b[text()='1800 100 641']";
	
}
