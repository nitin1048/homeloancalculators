
Feature: Home Loan Calculators and Tools

  @homeloanCalculator
  Scenario Outline: Validating the estimate on how much we borrow based on current income and existing financial commitments
    Given I am on home loans calculators tools page with <URL>
    Then I verify the header is displayed as Your details
    Then I verify the label is displayed as Application type
    Then I click on Single tab
    Then I verify the label is displayed as Number of dependants
    Then I select the 0 dependants 
    Then I verify the label is displayed as Property you would like to buy
    Then I select the Home to live in
    Then I verify the header is displayed as Your earnings
    Then I verify the label is displayed as Your income (before tax)
    Then I enter Your income (before tax) as <income>  
    Then I verify the label is displayed as Your other income 
    Then I enter Your other income as <otherIncome> 
    Then I verify the header is displayed as Your expenses 
    Then I verify the label is displayed as Living expenses 
    Then I enter Living expenses as <livingExpenses>       
    Then I verify the label is displayed as Current home loan repayments
    Then I enter Current home loan repayments as <current_Home_Loan_Repayments>   
    Then I verify the label is displayed as Other loan repayments
    Then I enter Other loan repayments  as <Other_loan_repayments>   
    Then I verify the label is displayed as Other commitments
    Then I enter Other commitments  as <Other_commitments> 
    Then I verify the label is displayed as Total credit card limits
    Then I enter Total credit card limits as <Total_credit_card_limits>
    Then I check the borrowing estimate 
    Then I take home_Loan_Calculator screenshots
    Then I verify the cleared data of field
  
    Examples: 
      |                                   URL                                     | income   |otherIncome |livingExpenses|current_Home_Loan_Repayments|Other_loan_repayments|Other_commitments|Total_credit_card_limits|
      | https://www.anz.com.au/personal/home-loans/calculators-tools/much-borrow/ | 80000    |10000       |500           |0                           |100                  |0                |10000|

      
  @homeloanCalculator
  Scenario Outline: Validate the message after entering only $1 for Living expenses, and leaving all other fields as zero
    Given I am on home loans calculators tools page with <URL>
    Then I verify the header is displayed as Your details
    Then I verify the label is displayed as Application type
    Then I click on Single tab
    Then I verify the label is displayed as Number of dependants
    Then I select the 0 dependants 
    Then I verify the label is displayed as Property you would like to buy
    Then I select the Home to live in
    Then I verify the header is displayed as Your earnings
    Then I verify the label is displayed as Your income (before tax)
    Then I enter Your income (before tax) as <income>  
    Then I verify the label is displayed as Your other income 
    Then I enter Your other income as <otherIncome> 
    Then I verify the header is displayed as Your expenses 
    Then I verify the label is displayed as Living expenses 
    Then I enter Living expenses as <livingExpenses>       
    Then I verify the label is displayed as Current home loan repayments
    Then I enter Current home loan repayments as <current_Home_Loan_Repayments>   
    Then I verify the label is displayed as Other loan repayments
    Then I enter Other loan repayments  as <Other_loan_repayments>   
    Then I verify the label is displayed as Other commitments
    Then I enter Other commitments  as <Other_commitments> 
    Then I verify the label is displayed as Total credit card limits
    Then I enter Total credit card limits as <Total_credit_card_limits> 
    Then I verify the message  
  
    Examples: 
      |                                      URL                                   | income   | otherIncome |livingExpenses|current_Home_Loan_Repayments|Other_loan_repayments|Other_commitments|Total_credit_card_limits|
      |  https://www.anz.com.au/personal/home-loans/calculators-tools/much-borrow/ |   0      |    0        |       1      |             0              |           0         |        0        |           0            |
      