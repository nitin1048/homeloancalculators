package runners;

import java.io.File;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		          features= {"src/test/java/features/homeLoanCalculator.feature"}, 
				  glue= {"core","stepDefinitions"},
	              plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" }, 
				  monochrome = true,
                  tags= {"@homeloanCalculator"}
                 )
public class TestRunner  {

	@AfterClass
	    public static void report() throws Exception {
	        Reporter.loadXMLConfig(new File("configs/extent-config.xml"));
	        Reporter.setSystemInfo("User", System.getProperty("user.name"));
	        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
	        Reporter.setSystemInfo("Machine", "Windows 7 " + "64 Bit");
	        Reporter.setSystemInfo("Java Version", "1.8.0_131");
	        Reporter.setSystemInfo("Maven", "3.6.0");
	        Reporter.setSystemInfo("Selenium", "2.53.0");

	}
	 
}

