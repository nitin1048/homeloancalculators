package stepDefinitions;

import static core.Base.driver;
import static homeLoanCalculator.HomeLoan.*;
import static core.Action.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TestHomeLoanCalculator {

	@Given("^I am on home loans calculators tools page with (.*)$")
	public void i_am_on_home_loans_calculators_tools_page_with(String url) throws Exception {
		loginPage(driver, url);
	}

	@Then("^I close the browser$")
	public void i_close_the_browser() throws Exception {
		driver.quit();
	}
	
	@Then("^I take (.*) screenshots$")
	public void i_take_screenshots(String screenShotName) throws Throwable {
		takeScreenShots(driver, screenShotName);
		
	}


	@Then("^I click on Single tab$")
	public void i_click_on_Single_tab() throws Exception {
		selectSingleTabInHomeLoanCalculatorPage(driver);
	}

	@Then("^I select the (.*) dependants$")
	public void i_select_the_dependants(int arg1) throws Exception {
		selectDependentInHomeLoanCalculatorPage(driver);
	}

	@Then("^I select the Home to live in$")
	public void i_select_the_Home_to_live_in() throws Exception {
		selectHomeToLiveInTab(driver);
	}

	@Then("^I enter Your income \\(before tax\\) as (.*)$")
	public void i_enter_Your_income_before_tax_as(String myincome) throws Exception {
		enterAmountInYourIncomeField(driver, myincome);
	}

	@Then("^I enter Your other income as (.*)$")
	public void i_enter_Your_other_income_as(String otherIncome) throws Exception {
		enterAmountInOtherIncomeField(driver, otherIncome);
	}

	@Then("^I verify the header as Your expenses$")
	public void i_verify_the_header_as_Your_expenses() throws Exception {
		verifyHeaderYourExpenses(driver);

	}

	@Then("^I enter Living expenses as (.*)$")
	public void i_enter_Living_expenses_as(String livingExpenses) throws Exception {
		enterDataInLivingExpensesField(driver, livingExpenses);
	}

	@Then("^I enter Current home loan repayments as (.*)$")
	public void i_enter_Current_home_loan_repayments_as(String homeloans) throws Exception {
		enterAmountInCurrentHomeLoanRepaymentsField(driver, homeloans);

	}

	@Then("^I enter Other loan repayments  as (.*)$")
	public void i_enter_Other_loan_repayments_as(String otherloans) throws Exception {
		enterAmountInOthetrsRepaymentsField(driver, otherloans);
	}

	@Then("^I enter Other commitments  as (.*)$")
	public void i_enter_Other_commitments_as(String otherCommitments) throws Exception {
		enterAmountInOthetCommitmentField(driver, otherCommitments);
	}

	@Then("^I enter Total credit card limits as (.*)$")
	public void i_enter_Total_credit_card_limits_as(String creditCardAmount) throws Exception {
		enterAmountInTotalCreditCardLimitsField(driver, creditCardAmount);
	}

	@Then("^I check the borrowing estimate$")
	public void i_check_the_borrowing_estimate() throws Exception {
		clickOnWorkOutHowMuchICouldBorrowButton(driver);
		verifyTheBorrowEstimate(driver);
	}

	@Then("^I verify the header is displayed as (.*)$")
	public void i_verify_the_header_is_displayed_as_Your_details(String elementValue) throws Exception {
		verifyAllHeaderInHomePage(driver, elementValue);
	}

	@Then("^I verify the label is displayed as (.*)$")
	public void i_verify_the_label_is_displayed_as_Application_type(String elementValue) throws Exception {
		verifyAllLabelInHomePage(driver, elementValue);
	}

	@Then("^I click on start over button$")
	public void i_click_on_start_over_button() throws Exception {
		verifyAmountAfterClearingFields(driver);
		clickOnStartOverIcon(driver);
	}

	@Then("^I verify the cleared data of field$")
	public void i_verify_the_cleared_data_of_field() throws Exception {
		verifyAmountAfterClearingFields(driver);
	}

	@Then("^I verify the message$")
	public void i_verify_the_message() throws Exception {
		clickOnWorkOutHowMuchICouldBorrowButton(driver);
		verifytheMessage(driver);
	}
}
